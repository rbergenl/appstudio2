module.exports = function(config){
    config.set({

        basePath : './',

        files : [
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-ui-router/release/angular-ui-router.js',
            'app/bower_components/angular-mocks/angular-mocks.js',
            'app/app.js',
            'app/components/**/*.js',
            'app/partials/**/*.js'
        ],

        exclude : [
            '**/*_e2e.js',
            '**/*_pageobject.js'
        ],

        autoWatch : true,

        frameworks: ['jasmine'],

        browsers : ['Chrome'],

        plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-html-reporter'
        ],

        reporters: ['progress', 'html', 'junit'],

        // the default configuration
        htmlReporter: {
            outputDir: 'reports/karma_html', // where to put the reports
            templatePath: null, // set if you moved jasmine_template.html
            focusOnFailures: true, // reports show failures on start
            namedFiles: false, // name files instead of creating sub-directories
            pageTitle: null, // page title for reports; browser info by default
            urlFriendlyName: false, // simply replaces spaces with _ for files/dirs

            // experimental
            preserveDescribeNesting: false, // folded suites stay folded
            foldAll: false // reports start folded (only with preserveDescribeNesting)
        },


        junitReporter : {
            outputFile: 'reports/karma_xml/unit.xml',
            suite: 'unit'
        }

    });

};