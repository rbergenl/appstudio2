'use strict';
/**
 * Created by Robin on 4/15/2015
 *
 */
(function () {

	var app = angular
				.module("projectResourceMock",
						["ngMockE2E"]);
	
	app.run(function($httpBackend){
		var projects = [
			{
				"productId": 1,
				"name": "Leaf Rake",
				"productCode": "GDN-0011",
                "description": "Blabla",
				"releaseDate": "March 19, 2009",
				"price": 19.95,
				"cost": 10,
                "tags": ["test", "more"]
			},
			{
				"productId": 2,
				"name": "Hammer",
				"productCode": "TBX-0048",
                "description": "Blabla",
				"releaseDate": "May 21, 2013",
				"price": 8.99,
				"cost": 3,
                "tags": ["test"]
			}
		];
		
		var projectUrl = "/api/projects";
		$httpBackend.whenGET(projectUrl).respond(projects);
/*
		var editingRegex = new RegExp(productUrl + "/[0-9][0-9]*", '');
		$httpBackend.whenGET(editingRegex).respond(function(method, url){
			var product = {"productId": 0};
			var parameters = url.split('/');
			var length = parameters.length;
			var id = parameters[length - 1];

			if (id > 0) {
				for (var i = 0; i < products.length; i++) {
					if (products[i].productId == id) {
						product = products[i];
						break;
					}
				}
			}
			return [200, product, {}];
		});

        $httpBackend.whenPOST(productUrl).respond(function(method, url, data){
           var product = angular.fromJson(data);
            if (!product.productId) {
                //new product Id
                product.productId = products[products.length - 1].productId + 1;
                products.push(product);
            }
            else {
                // Updated product
                for (var i = 0; i < products.length; i++){
                    if (products[i].productId == product.productId) {
                        products[i] = product;
                        break;
                    }
                }
            }
            return [200, product, {}];
        });
*/
		//pass through any requests for application files
		$httpBackend.whenGET().passThrough();
	});
	
}());