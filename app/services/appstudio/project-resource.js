/**
 * Created by Robin on 4/15/2015
 * @factory:
 * @description:
 * @documentation: api.domain.com/project
 */
(function () {
	
	'use strict';

	angular
		.module("common.services",
		["ngResource"]);


	angular
		.module("common.services")
		.factory("projectResource",
				["$resource",
				 projectResource]);

	function projectResource ($resource) {
		return $resource("/api/projects/:projectId");
	}
	
}());