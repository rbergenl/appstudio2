/**
 * Created by Robin on 4/15/2015
 */
(function () {

    'use strict';

    angular
        .module("appStudio")

        .config(["$stateProvider",
                "$urlRouterProvider",
                ProjectListState])

        .controller("ProjectListController",
                     ["projectResource",
                    ProjectListController]);

    function ProjectListState ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("projectList", {
                url: "/projects",
                templateUrl: "partials/project/list-view/project-list-template.html",
                controller: "ProjectListController as vm",
                resolve: {
                    projectResource: "projectResource",

                    projects: function (projectResource) {
                        return projectResource.query(function (response) {
                                //no code needed for success
                            },
                            function (response) {
                                console.log(response);
                                if (response.status == 404) {
                                    toastr.error("Error accessing resource: " +
                                    response.config.method + " " +
                                    response.config.url);
                                } else {
                                    toastr.error(response.statusText);
                                }
                            }).$promise;
                    }
                }
            })
    }


    function ProjectListController (projectResource) {
        var vm = this;

        projectResource.query(function(data){
            vm.projects = data;
        });

        //show default no image
        vm.showImage = false;

        vm.toggleImage = function() {
            vm.showImage = !vm.showImage;
        };
    }

}());