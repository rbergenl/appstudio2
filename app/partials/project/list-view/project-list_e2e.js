/*global describe: false */
/*global it: false */
'use strict';
/**
 * As {productOwner}
 * I want {to see the product backlog}
 * So that {I can verify if the user stories are complete and prioritized}
 * @class ProductOwner
 * @constructor
 * @epic ProductOwner(also)
 *
 */

(function () {

describe('project list', function () {
    it('shows list', function () {
        browser.get('/');

    });
});


    /*
    describe('angularjs homepage', function () {
        it('should greet the named user', function () {
            var angularHomepage = new AngularHomepage();
            angularHomepage.get();

            angularHomepage.setName('Julie');

            expect(angularHomepage.greeting.getText()).toEqual('Hello Julie!');
        });
    });
*/
/*
    describe("project list")
    {

        describe("container")
        {

            it("displays a list with projects")
            {
                Given(function () {

                };
                When(function () {

                };
                Then(function () {

                };
            }

        }
    }
     */
})();

/*
 'use strict';

 var AngularPage = require('../pages/angular.page.js');

 describe('angularjs homepage', function () {
 var page;

 beforeEach(function () {
 page = new AngularPage();
 });

 it('should greet the named user', function () {
 page.typeName('Julie');
 expect(page.greeting).toEqual('Hello Julie!');
 });

 describe('todo list', function () {
 it('should list todos', function () {
 expect(page.todoList.count()).toEqual(2);
 expect(page.todoAt(1)).toEqual('build an angular app');
 });

 it('should add a todo', function () {
 page.addTodo('write a protractor test');
 expect(page.todoList.count()).toEqual(3);
 expect(page.todoAt(2)).toEqual('write a protractor test');
 });
 });
 */

/*
 describe('PhoneCat App', function () {

 describe('Phone list view', function () {

 beforeEach(function () {
 browser.get('app/index-example.html');
 });


 it('should filter the phone list as a user types into the search box', function () {

 var phoneList = element.all(by.repeater('phone in phones'));
 var query = element(by.model('query'));

 expect(phoneList.count()).toBe(3);

 query.sendKeys('nexus');
 expect(phoneList.count()).toBe(1);

 query.clear();
 query.sendKeys('motorola');
 expect(phoneList.count()).toBe(2);
 });
 });
 });
 */