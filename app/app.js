(function () {

    'use strict';

    /**
     * @ngdoc overview
     * @name appStudio
     *
     * @requires ui.router common.services projectResourceMock
     *
     * @description
     * # appStudio
     *
     * ## The main module for ui.router
     * There are several sub-modules included with the ui.router module, however only this module is needed
     * as a dependency within your angular app. The other modules are for organization purposes.
     *
     * The modules are:
     * * ui.router - the main "umbrella" module
     * * ui.router.router -
     *
     * *You'll need to include **only** this module as the dependency within your angular app.*
     *
     * <pre>
     * <!doctype html>
     * <html ng-app="myApp">
     * <head>
     *   <script src="js/angular.js"></script>
     *   <!-- Include the ui-router script -->
     *   <script src="js/angular-ui-router.min.js"></script>
     *   <script>
     *     // ...and add 'ui.router' as a dependency
     *     var myApp = angular.module('myApp', ['ui.router']);
     *   </script>
     * </head>
     * <body>
     * </body>
     * </html>
     * </pre>
     */
    var app = angular
        .module("appStudio",
            ["ui.router",
            "common.services",
            "projectResourceMock"]);


    //configure service $stateProvider
    app.config(["$stateProvider",
        "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider){
            //define standard route
            $urlRouterProvider.otherwise("/");

            //define the states
            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "partials/home/home-view.html"
                })
                .state("projectAnalytics", {
                    url: "/projects",
                    templateUrl: "partials/project/project-list-view.html",
                    controller: "ProjectListController as vm"
                })

        }]);



}());