'use strict';
/**
 * @ngdoc overview
 * @name myApp.version.version-directive
 *
 * @description
 * # myApp.version.version-directive sub-module
 *
 * This module is a dependency of other sub-modules. Do not include this module as a dependency
 * in your angular app.
 *
 */
angular.module('myApp.version.version-directive', [])
    /**
     * @ngdoc directive
     * @name appVersion
     * @description
     * A map of examples parsed out of the doc content, keyed on
     */
    .directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }]);