/*
'use strict';

describe('my app', function() {

    browser.get('/');

    it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
     //   expect(browser.getLocationAbsUrl()).toMatch("/view1");
    });


    describe('view1', function() {

        beforeEach(function() {
            browser.get('index.html#/project');
        });


        it('should render view1 when user navigates to /view1', function() {
            expect(1).toBe(1);
        //    expect(element.all(by.css('[ng-view] p')).first().getText()).
         //       toMatch(/partial for view 1/);
        });

    });
});
*/
describe('angularjs homepage', function() {
    it('should greet the named user', function() {
        browser.get('http://www.angularjs.org');

        element(by.model('yourName')).sendKeys('Julie');

        var greeting = element(by.binding('yourName'));

        expect(greeting.getText()).toEqual('Hello Julie hello!');
    });

    describe('todo list', function() {
        var todoList;

        beforeEach(function() {
            browser.get('http://www.angularjs.org');

            todoList = element.all(by.repeater('todo in todos'));
        });

        it('should list todos', function() {
            expect(todoList.count()).toEqual(2);
            expect(todoList.get(1).getText()).toEqual('build an angular app');
        });

        it('should add a todo', function() {
            var addTodo = element(by.model('todoText'));
            var addButton = element(by.css('[value="add"]'));

            addTodo.sendKeys('write a protractor test');
            addButton.click();

            expect(todoList.count()).toEqual(3);
            expect(todoList.get(2).getText()).toEqual('write a protractor test');
        });
    });
});